-- <array>SelectObjectByMaterial - returns array of objects a material is assigned to
fn SelectObjectByMaterial reqMatEditOpen materialIndex = (
	objarr = #()
	
	-- Select ALL objects with this material
	if MatEditor.isOpen() or (reqMatEditOpen == false) then (
		if materialIndex > 0 and materialIndex < 25 then (	-- start index check
			objarr = for o in objects where o.material == meditMaterials[materialIndex] collect o
			-- Check if the obj is part of a group
			print(objarr as string)
			for obj in objarr where isGroupMember obj AND (NOT isOpenGroupMember obj) do
			(

			  	par = obj.parent
			  	while par != undefined do
			  	(

			  		if isGroupHead par then 
			  		(
			  			setGroupOpen par true
			  			par = undefined
			  		)
			  		else par = par.parent
				)
			)				
		) -- end index check
	) else	-- Material Editor isn't open
		messageBox "Material Editor is not open" beep:false
		
	select objarr
)
SelectObjectByMaterial false (medit.getActiveMtlSlot())