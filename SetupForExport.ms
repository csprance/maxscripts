---------------------------------
/*----------------------------
- Set up for export from modo
- This is a script I wrote to export an FBX from modo
- And click one button to have it set up in Max
- Chris Sprance
- Entrada Interactive
- csprance@entradainteractive.com
------------------------------*/
----------------------------------
-- Proxies must be named proxy_name

-- What it Does
-- Import The Object
-- Reset the Xforms on the Object
-- Combine all the materials into one material
-- Set up the material ids on the objects 
-- Set all standard materials to CryTek shaders
-- If the material name is proxy_ make it a Physical Proxy No Draw.
-- Add the Top Locator to the Exporter dialog
-- Save the File
-- Export

------------------------------
/*----------------------------
- Start Functions
*/----------------------------
------------------------------
-- this function selects polygons assigned to mat and assigns a matid of count
-- mat is the mat to select the polys by and count is the mat id to assign to the polys
function assignMatId mat count = (
	-- select the polys assigned to mat

	--assign selected polys a matid of count

)
-- this will return only standard materials in the scene including diving down into Multimaterials to grab the submats
-- params selobj "Objects to process and return unique materials from"
-- returns #(array of unique materials)
function getUniqueMats selobj = 
(

		-- init our final array
		final = #()

		--if it's not a dummy grab it's material and collect it into matArray
		matArray = for mat in selobj where classOf mat != Dummy collect mat.material

		-- pull any submats from matArray into final
		for mat in matArray where classOf mat == Multimaterial do 
		(
			for submat in mat do 
				append final submat
		)

		--add our matArray to our final
		for mat in matArray where classOf mat == Standardmaterial do 
			append final mat
		-- send our final array back after we make it unique
		--return makeUniqueArray final
		return uniquifyArray final
)

-- this function goes through an array of materials and removes any elements that have the same name.
-- params arr "Array of materials to look through"
-- returns a new array with all unique objects
function uniquifyArray arr = 
(
	-- get the length of the arr
	curnum = arr.count
	for i in arr do
	(
		-- if finditem is greater than zero and equal to the current items index remove the element
		if (findItem arr i > 0 ) and ( finditem arr i == curnum ) do
		(
			--print("removing element " + i as string)
			--delete our item from arr and use findItem to find the index of the item 
			deleteItem arr (findItem arr i)
		)
		--this cycles us backwards through our loop
		curnum -= 1
	)
	return arr
)

--reset xform function
-- params selobj "Objects to reset xform on and recurse through children perform resets as well then relinking"
-- returns nothing - This function performs an action on a set of objects
function ResetXFormLinked selobj = 
(

	for obj in selobj do (
		children = #()

		for child in obj.children do (
			append children child
			)

		for child in children do (
			child.parent = undefined
			)

		collapseStack obj
		ResetXForm obj
		collapseStack obj

		if children.count >= 1 then (
			for child in children do (
				child.parent = obj
				)
			)
		)
)

--function to create the Multimaterial and assign the correct standardmats
-- params mats #(array of unique materials)
-- Nothing This function creates a multimmaterial places it in the material editor as the active material and populates it with mats from mats param
function setupMats mats = 
(
	-- first we create our Multimaterial and set it in the material editor
	finalmat = Multimaterial numsubs:mats.count
	--add it to slot 1
	medit.PutMtlToMtlEditor finalmat 1 
	--set slot 1 as the active slot
	medit.SetActiveMtlSlot 1 true 
	-- open the mat editor
	MatEditor.open() 
	-- creat the dialog to ask for the material name
	createDialog MaterialName width:200 modal:true 
	-- Add The materials into the submat slots
	count = 1
	-- iterate through the mats and add them all to the new material we just created
	for mat in mats do 
	(
		-- call our function to assign the mat id correctly mat is the mat to select the polys by and count is the mat id to assign to the polys
		--assignMatId mat count
		--Place the mat from mats into the materialList[count]
		finalmat.materialList[count] = mat
		-- set the mat to a crytek Shader
		finalmat.materialList[count].shaderType = 2
		-- if it has physics in the name set it to Physicalize
		if findString mat.name "proxy_" != undefined and findString mat.name "proxy_" > 0 do
		(
			-- set it physicalize
			finalmat.materialList[count].physicalizeMaterial = on
			-- set it to Physical Proxy No Draw
			finalmat.materialList[count].surfaceName = "Physical Proxy (NoDraw)"
			-- Switching from the regular blinn removes transparency turn it back on
			finalmat.materialList[count].alphaBlend = on
			-- set the opacity to something low
			finalmat.materialList[count].opacity = 10
			-- change the color to red
			--finalmat.materialList[count].Diffuse = color 255 0 0

		)
		count += 1
	)
)

-- function to set the Smoothing Groups by UV islands on selected object 
-- doesn't return anything this function preforms and action on the selected object
function smoothByUvIslands=(
	clearListener();
	if (getCommandPanelTaskMode() != #modify)then(--make sure we are in the modify panel section
		setCommandPanelTaskMode #modify;
	)
	if (selection.count == 1)then(--at least an object selected
		local obj = selection[1]; 
		local uv = modPanel.getCurrentObject();
		
		if (classof(uv) != Unwrap_UVW)then(
			modPanel.addModToSelection (Unwrap_UVW ()) ui:on;
			uv = modPanel.getCurrentObject();
		)
		
		uv.unwrap.edit();
		uv.unwrap.edit();
		uv.unwrap2.setTVSubObjectMode(3);

		local totalFaces = uv.unwrap.numberPolygons();
		
		local faceElemArray = #();
		for f=1 to totalFaces do (
			faceElemArray[ f ] = 0;
		)
		local elem = #();
		--with redraw off;
		for f=1 to totalFaces do (
			if faceElemArray[ f ] == 0 then (
				uv.unwrap2.selectFaces  #{ f };
				uv.unwrap2.selectElement();
				local elemFaces = uv.unwrap2.getSelectedFaces() as array;
				
				append elem (uv.unwrap2.getSelectedFaces());
				for i in elemFaces do (
					faceElemArray[ i ] = elem.count; -- Mark these vertices with their element number in vertElemArray.
				)
			)
		)
		
		print("num shells: "+elem.count as string+"\t"+totalFaces as string);
		
		modPanel.addModToSelection (Edit_Poly ()) ui:on;
		obj.modifiers[#Edit_Poly].autoSmoothThreshold = 180	
		for e in elem do(
			obj.modifiers[#Edit_Poly].SetSelection #Face e;	
			obj.modifiers[#Edit_Poly].ButtonOp #Autosmooth	
		)	
	)
	
)

--function to reset the scene and import the files
function resetImport=(
		-- reset
	resetMaxFile #noprompt
	-- make sure the FBX plugin is loaded
	pluginManager.loadClass FbxImporter
	-- set up our FBX settings
	FBXsettings = #(
					#("Animation",False),
					#("AxisConversion",True),
					#("BakeAnimationLayers",False),
					#("Cameras",False),
					#("ConvertUnit","cm"),
					#("FillTimeline",False),
					#("FilterKeyReducer",False),
					#("FilterKeySync",False),
					#("GenerateLog",False),
					#("ImportBoneAsDummy",False),
					#("KeepFrameRate",False),
					#("Lights",False),
					#("Markers",False),
					#("Mode",#create),
					#("PointCache",False),
					#("ScaleConversion",True),
					#("ScaleFactor",1),
					#("Shape",False),
					#("Skin",False),
					#("SmoothingGroups",True),
					#("UpAxis","Z")
				)
	-- set our FBX settings
	for s in FBXsettings do FbxImporterSetParam s[1] s[2]

	-- import our file
	ImportFile "D:\\miscreated2\\GameSDK\\Objects\\materialtest\\test_from_modo.fbx" #noprompt
)

--Rollout to get the material name 
-- This is the code that creates a dialog to name the material. This will probably be replaced by instead grabbing the root object and using it's name
rollout MaterialName "Material Name"
(
	edittext mmname "Enter Material Name"
	button cont "Continue"
	on cont pressed do
	(
		meditMaterials[1].name = mmname.text
		DestroyDialog MaterialName 
	)
)

---------------------------------
/*----------------------------
-
-
- Main Code Execution Area
-
-
-
------------------------------*/
----------------------------------


-- reset everything and import our objects
resetImport()

-- reset the xforms on everything in the scene
ResetXFormLinked $*

-- run the smoothByUvIslands
for obj in $* do if classOf obj == Editable_Poly do( select obj; smoothByUvIslands())

-- set up my mats using the return from getUniqueMats
setupMats (getUniqueMats $*)

--



--Apply material to mesh
--$*.material = meditMaterials[1]
